var fetchdData;
var theTimeOut = [];
var currentStoryImages;
const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];
const dateIs = new Date();
const currentMonth = monthNames[dateIs.getMonth()];

$(document).ready(function () {
    console.log('infinity war shattered me from inside');
    callDicHead();
    handleCardClick();
    handleOverlayClick();
    handleForwardClick();
    handleBackwardClick();
    handleYeahClick();
    handleMaybeClick();
    handleNaahClick();

});

function callDicHead() {
    $
        .ajax({method: 'Get', dataType: 'json', url: 'scripts/dic-head.json'})
        .done(function (data) {

            // If successful
            let dicData = data['dics'];
            fetchdData = dicData;
            placeCards(dicData);

        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            // If fail
            console.log(textStatus + ': ' + errorThrown);
        });

}

function placeCards(dicData) {
    for (let i = 0; i < dicData.length; i++) {
        let oneDicData = dicData[i];

        let oneCardObj = {}
        oneCardObj.cityId = i;
        oneCardObj.city = oneDicData['cityName'];
        oneCardObj.country = oneDicData['countryName'];
        oneCardObj.tags = oneDicData['knownFor'].split(',');
        oneCardObj.bg = oneDicData['bgImage'];
        oneCardObj.weather = oneDicData['currentWeather'];
        let iCardHtml = makeCards(oneCardObj);
        $(".boxContainer").append(iCardHtml);

        let cw = $('.boxImgBox').width();
        $('.boxImgBox').css({
            'height': (cw - 50) + 'px'
        });

    }

}

function makeCards(oneCardObj) {

    let cardHtml = `<div class="box ${oneCardObj.country}">
            <div class="boxImgBox" data-city="${oneCardObj.cityId}">
                <img class="boxImg" src="${oneCardObj.bg}" alt="">
                <div class="boxTitleButton">
                    <div class="boxTitle">${oneCardObj.city}</div>
                    <div class="boxButton">
                        <img src="imgs/play.png">
                    </div>
                </div>
                <div class="boxWeather">
                    <span class="boxWeatherMonth">${currentMonth} : </span>
                    <img src="imgs/${oneCardObj.weather}.png" style="width: 22px;height: 22px;">
                </div>
                <div class="UserTag" data-cityToTag="${oneCardObj.cityId}"></div>
            </div>
            <div class="boxtags">
                ${makeBoxTags(
        oneCardObj.tags
    )}
            </div>
        </div> `;

    return cardHtml;
}

function handleOverlayClick() {

    $(".overlay, .storyClose").click(function () {
        $('.overlay').hide();
        $('.storyScreen').hide();
        $('.rolling').hide();
    });

}

function handleCardClick() {
    $('body').on('click', 'div.boxImgBox', function () {
        let cityId = $(this).attr('data-city');
        showStory(cityId);
    });

}

function clearTheTimeout() {
    theTimeOut.forEach(function (timer) {
        clearTimeout(timer);
    });
}

function hideClickSpace() {
    $('.storySkip').hide();
}
function showClickSpace() {
    $('.storySkip').show();
}

function showStory(cityId) {

    var time = 0;
    $(".storyBars").html('');
    $('.overlay').show();
    $('.storyScreen').show();
    $('.storyMetaContainer').hide();
    $('.storyMeta').removeClass('storyMetaShow');
    $('.rolling').show();
    $('.storyDesc').html('Loading images . . .');
    $('.storyScreen').css(
        'background-image',
        'url(' + fetchdData[cityId].bgImage + ')'
    );
    hideClickSpace();
    let storyImages = fetchdData[cityId]['categories'][0]['images'];
    currentStoryImages = storyImages;

    let storyLength = storyImages.length;
    let barWidth = (100 - (storyLength * 2)) / storyLength;
    for (let p = 0; p < storyLength; p++) {
        let bar = `<div class="storyBar" data-idd="${p}" style="width:${barWidth}%"></div>`;
        $(".storyBars").append(bar);
    }

    clearTheTimeout();

    var ImgArray = [];

    for (let l = 0; l < storyLength; l++) {
        ImgArray.push(storyImages[l].url);
    }

    function preloadImages(urls, allImagesLoadedCallback) {

        var loadedCounter = 0;
        var toBeLoadedNumber = urls.length;
        urls.forEach(function (url) {
            preloadImage(url, function () {
                loadedCounter++;
                console.log('image loaded: ' + loadedCounter);
                if (loadedCounter == toBeLoadedNumber) {
                    allImagesLoadedCallback();
                }
            });
        });
        function preloadImage(url, anImageLoadedCallback) {
            var img = new Image();
            img.src = url;
            img.onload = anImageLoadedCallback;
        }
    }

    preloadImages(ImgArray, imagesLoaded);

    let cityName = fetchdData[cityId].cityName;
    let cityDesc = fetchdData[cityId].description;

    let cityRecommendedFor = fetchdData[cityId].recommendedFor;
    cityRecommendedFor = cityRecommendedFor.split(',');

    let cityKnownFor = fetchdData[cityId].knownFor;
    cityKnownFor = cityKnownFor.split(',');

    let cityCurrentWeather = fetchdData[cityId].currentWeather;
    let cityBestTimeToGo = fetchdData[cityId].bestTimeToGo;
    let cityCostInfo = fetchdData[cityId].costInfo;
    let cityFoodNdDrinksInfo = fetchdData[cityId].foodNdDrinksInfo;
    let cityGettingThereInfo = fetchdData[cityId].gettingThereInfo;

    $('#metaTitle').html(cityName);
    $('#askUserCountry').html(cityName);
    $('#metaDesc').html(cityDesc);
    $('#bestTimeVisit').html(cityBestTimeToGo);
    $('#metaTagRecommend').html(makeMetaTags(cityRecommendedFor));
    $('#metaTagKnown').html(makeMetaTags(cityKnownFor));
    $('#metaCostInfo').html(cityCostInfo);
    $('#metaFoodInfo').html(cityFoodNdDrinksInfo);
    $('#metaGetThereInfo').html(cityGettingThereInfo);
    $('#metaWeatherMonth').html('Weather in ' + currentMonth);
    $('#metaWeatherIcon').attr('src', 'imgs/' + cityCurrentWeather + '.png');

    $('.askUserButt').attr('data-toCity', cityId);

    function imagesLoaded() {
        startStory();
    }

}

function startStory() {
    $('.rolling').hide();
    showClickSpace();
    time = 0;
    for (let k = 0; k <= currentStoryImages.length; k++) {
        theTimeOut.push(setTimeout(function () {
            if (k == (currentStoryImages.length)) {
                $('.storyMetaContainer').show();
                $('.storyMeta').addClass('storyMetaShow');
                hideClickSpace();
            } else {
                $('#storyGoBack').attr('data-story', k - 1);
                $('#storyGoForward').attr('data-story', k + 1);
                $("div[data-idd='" + k + "']").addClass("storyBarFill");
                $('.storyDesc').html(currentStoryImages[k].description.replace(/U\+/g, "&#x"));
                $('.storyScreen').css(
                    'background-image',
                    'url(' + currentStoryImages[k].url + ')'
                );
            }
        }, time));
        time += 2000;
    }
}

function handleForwardClick() {

    $('body').on('click', '#storyGoForward', function () {
        let gotoStory = parseInt($(this).attr('data-story'));
        skipStoryNext(gotoStory);
        // console.log(gotoStory);
    });

}
function handleBackwardClick() {

    $('body').on('click', '#storyGoBack', function () {
        let gotoStory = parseInt($(this).attr('data-story'));

        if (gotoStory < 0) {
            return;
        }
        skipStoryBack(gotoStory);
        // console.log(gotoStory);
    });

}

function skipStoryNext(gotoStory) {

    if (currentStoryImages.length <= gotoStory) {
        return;
    }
    clearTheTimeout();
    time = 0;

    for (let temp = 0; temp < gotoStory; temp++) {
        $("div[data-idd='" + temp + "']").removeClass("storyBarFill");
        $("div[data-idd='" + temp + "']").addClass("storyBarFillAlready");
        // alert(temp);
    }

    // $('.storyBar').addClass('storyBarFillAlready');
    for (let k = gotoStory; k <= currentStoryImages.length; k++) {
        theTimeOut.push(setTimeout(function () {
            if (k == (currentStoryImages.length)) {
                $('.storyMetaContainer').show();
                $('.storyMeta').addClass('storyMetaShow');
                hideClickSpace();
            } else {
                $('#storyGoBack').attr('data-story', k - 1);
                $('#storyGoForward').attr('data-story', k + 1);
                $("div[data-idd='" + k + "']").addClass("storyBarFill");
                $('.storyDesc').html(currentStoryImages[k].description.replace(/U\+/g, "&#x"));
                $('.storyScreen').css(
                    'background-image',
                    'url(' + currentStoryImages[k].url + ')'
                );
            }
        }, time));
        time += 2000;
    }

}

function skipStoryBack(gotoStory) {

    clearTheTimeout();
    time = 0;

    $('.storyBar').removeClass('storyBarFill');
    $('.storyBar').removeClass('storyBarFillAlready');

    for (let temp = 0; temp < gotoStory; temp++) {
        $("div[data-idd='" + temp + "']").removeClass("storyBarFill");
        $("div[data-idd='" + temp + "']").addClass("storyBarFillAlready");
    }

    for (let k = gotoStory; k <= currentStoryImages.length; k++) {
        theTimeOut.push(setTimeout(function () {
            if (k == (currentStoryImages.length)) {
                $('.storyMetaContainer').show();
                $('.storyMeta').addClass('storyMetaShow');
                hideClickSpace();
            } else {
                $('#storyGoBack').attr('data-story', k - 1);
                $('#storyGoForward').attr('data-story', k + 1);
                $("div[data-idd='" + k + "']").removeClass("storyBarEmptyAlready");
                $("div[data-idd='" + k + "']").addClass("storyBarFill");
                $('.storyDesc').html(currentStoryImages[k].description.replace(/U\+/g, "&#x"));
                $('.storyScreen').css(
                    'background-image',
                    'url(' + currentStoryImages[k].url + ')'
                );

            }
        }, time));
        time += 2000;
    }

}

function makeMetaTags(arr) {
    let allTags = '';
    arr.forEach(element => {
        allTags += `<span class="metaTag">${element}</span>`;
    });

    return allTags;
}

function makeBoxTags(arr) {
    let allTags = '';
    arr.forEach(element => {
        allTags += `<span class="boxTag">${element}</span>`;
    });

    return allTags;
}

function handleYeahClick() {

    $(".askUserButt1").click(function () {
        // alert('d');
        let addToCity = parseInt($(this).attr('data-toCity'));
        $("div[data-cityToTag='" + addToCity + "']")
            .text('Yeah')
            .addClass("yeah")
            .removeClass('naah')
            .removeClass('maybe');
        $('.overlay').hide();
        $('.storyScreen').hide();
    });
}

function handleMaybeClick() {
    $(".askUserButt2").click(function () {
        let addToCity = parseInt($(this).attr('data-toCity'));
        $("div[data-cityToTag='" + addToCity + "']")
            .text('Maybe')
            .addClass("maybe")
            .removeClass('naah')
            .removeClass('yeah');
        $('.overlay').hide();
        $('.storyScreen').hide();
    });
}

function handleNaahClick() {
    $(".askUserButt3").click(function () {

        let addToCity = parseInt($(this).attr('data-toCity'));
        $("div[data-cityToTag='" + addToCity + "']")
            .text('Naah!')
            .addClass("naah")
            .removeClass('yeah')
            .removeClass('maybe');
        $('.overlay').hide();
        $('.storyScreen').hide();

    });
}

$(window).on('resize', function () {
    let cw = $('.boxImgBox').width();
    $('.boxImgBox').css({
        'height': (cw - 50) + 'px'
    });
});

$(document).on('change', '#selectCountryId', function () {
    let cont = this.value;
    $('.countrySelected').text(cont);
    $('.box').hide();
    $('.' + cont + '').show();
});
